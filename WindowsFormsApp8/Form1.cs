﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace datosdwform
{
    public partial class Form1 : Form,Interface1
    {
        Stopwatch swra_local = new Stopwatch();
        
        public Form1()
        {
            InitializeComponent();
            otablaBindingSource.DataSource = Info.otablas;
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Tarea tarea = new Tarea();
                Info.otablas = new List<Otabla>();
                swra_local.Start();
                await tarea.Realizatarea(this) ;
                swra_local.Stop();
                  
                //MessageBox.Show(swra_local.Elapsed.ToString());

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        
        private void button2_Click(object sender, EventArgs e)
        {
            string textoOriginal = "Mañana será otro día";//transformación UNICODE
            string textoNormalizado = textoOriginal.Normalize(NormalizationForm.FormD);
            //coincide todo lo que no sean letras y números ascii o espacio
            //y lo reemplazamos por una cadena vacía.
            Regex reg = new Regex("[^a-zA-Z0-9 ]");
            string textoSinAcentos = reg.Replace(textoNormalizado, "");
            MessageBox.Show(textoSinAcentos); //muestra 'Manana sera otro dia'
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = Info.tablename;
            label2.Text = Info.actual.ToString();
            label3.Text = swra_local.Elapsed.ToString();
            otablaBindingSource.DataSource = Info.otablas;
            otablaBindingSource.MoveLast();
            otablaBindingSource.ResetBindings(false);
            if (Info.termino)
            {
                termino();
                Environment.Exit(Environment.ExitCode);
            }
            
        }

        public void pasardatos(Otabla s)
        {
            //int pos = otablaBindingSource.IndexOf(s);
            //if (pos == -1)
            //{

            //    otablaBindingSource.Add(s);
            //    otablaBindingSource.ResetBindings(false);
            //}
            //else
            //{
            //    if (s.status == "Terminado")
            //    {
            //        timer1_Tick(null, null);
            //        otablaBindingSource.Position = pos;
            //    //        otablaBindingSource.EndEdit();
            //    otablaBindingSource.ResetBindings(false);
            //    }

            //}
            //if (s.termino)
            //{
            //    termino();
            //}
           
            Application.DoEvents();
        }
        public void termino()
        {
            //foreach (Otabla o in otablaBindingSource)
            //{
            //    if (o.status != "Terminado")
            //    {
            //        if (o.actual < o.tablaservidor)
            //        {
            //            MessageBox.Show(string.Format("la tabla {0} no ha terminado", o.nombre));
            //            return;
            //        }
            //    }
            //}
            DataTable otablas = new DataTable();
            otablas.Columns.Add("id", typeof(Int32));
            otablas.Columns.Add("nombre", typeof(String));
            otablas.Columns.Add("tablasservidor", typeof(String));
            otablas.Columns.Add("status", typeof(String));
            otablas.Columns.Add("tlocal", typeof(String));
            otablas.Columns.Add("tarea", typeof(String));

            foreach (Otabla o in otablaBindingSource)
            {
                DataRow row = otablas.NewRow();
                row["id"] = o.id;
                row["nombre"] = o.nombre;
                row["tablasservidor"] = o.tablaservidor;
                row["status"] = o.status;
                row["tlocal"] = o.Tlocal;
                row["tarea"] = o.tarea;
                otablas.Rows.Add(row);
            }
            Tarea tabla = new Tarea();
            List<String> campos = new List<string>();
            foreach(DataColumn col in otablas.Columns)
            {
                campos.Add(col.ColumnName);
            }
            String filename= tabla.creaarchivo("ActualizacionDW", campos, "D:\\Data WareHouse\\Data DW\\");
            tabla.generaexcel(filename, otablas, "ActualizacionDW");
            //File.Delete(filename + ".csv");
            // Close();

        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            timer2.Enabled = false;
            button1_Click(sender, e);

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(Environment.ExitCode);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           // otablaBindingSource.DataSource =
        }
    }
}
