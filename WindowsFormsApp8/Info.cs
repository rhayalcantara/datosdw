﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace datosdwform
{
   public static class Info
    {
        
        public static Double actual { get; set; }
        public static String tablename { get; set; }
        public static List<Otabla> otablas { get; set; }
        public static bool termino { get; set; }
    }
}
