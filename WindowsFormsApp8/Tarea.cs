﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace datosdwform
{
    public class Tarea
    {
        public async Task Realizatarea(Interface1 interface1)
        {
            await Task.Run(async () =>
            {
                //SqlConnectionStringBuilder c = new SqlConnectionStringBuilder("Data Source = 192.168.7.5; Initial Catalog = Financ; Connection Timeout = 0; Integrated Security = True");
                SqlConnectionStringBuilder c = new SqlConnectionStringBuilder("Data Source = localhost; Initial Catalog = Financ;Connection Timeout=0; Integrated Security=True;");
                using (SqlConnection cn = new SqlConnection(c.ConnectionString))
                {
                    cn.Open();
                    using(SqlCommand cmd = new SqlCommand())
                    {
                        String sql = "SELECT CAST(table_name as varchar) TABLANAME  FROM INFORMATION_SCHEMA.TABLES where  TABLE_TYPE='BASE TABLE' order by 1";
                        cmd.Connection = cn;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = sql;
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {

                            DataTable dt = new DataTable();
                            da.Fill(dt);
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {

                               DataRow row = dt.Rows[i];
                               String tablaname = row["TABLANAME"].ToString();
                               await verificatabla(tablaname, interface1, cn);
                               
                            }
                            Info.termino = true;
                            da.Dispose();

                            
                        }


                    }
                }
                
            });
        }

        public async Task verificatabla(String tablaname, Interface1 form, SqlConnection cn)
        {
            List<String> sqlactulisadores = new List<string>();
            SqlConnectionStringBuilder c2 = new SqlConnectionStringBuilder("Data Source = localhost; Initial Catalog = FinancDW;Connection Timeout=0; Integrated Security=True;");
            String filename = string.Empty;
            Regex reg = new Regex("[^a-zA-Z0-9 ]");
            // cn.Open();
            SqlConnection cn2 = new SqlConnection(c2.ConnectionString);
            cn2.Open();

            using (SqlCommand cmd = new SqlCommand())
            {

                Info.tablename = tablaname;
                
                Otabla ot = new Otabla();
                Info.otablas.Add(ot);
                ot.nombre = tablaname;
                ot.tarea = "Procesando Datos";
                ot.status = "iniciada";
                form.pasardatos(ot);
                String sql = string.Format("select * from {0}", tablaname);
                cmd.Connection = cn;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandTimeout = 0;


                //obtiene los indices
                List<String> Ckeys = Camposkey(tablaname, cn);
                if (Ckeys.Count > 0)
                {

                    cmd.CommandText = sql + " where 1=2";

                    //CREAR EL TEMPORAL
                    DataTable temporal = new DataTable();
                    using (SqlDataAdapter datmp = new SqlDataAdapter(cmd))
                    {
                        datmp.Fill(temporal);
                    }
                

                    //CREAR EL ARCHIVO
                    List<String> campostabla = new List<string>();
                    List<String> campossalida = new List<string>();
                    List<string> campostablatype = new List<string>();
                    List<string> campostablainsert = new List<string>();
                    string comparafiled = string.Empty;
                    string igualafield = string.Empty;
                    List<string> campovalores = new List<string>();
                    foreach (DataColumn col in temporal.Columns)
                    {
                        
                        if (col.DataType.ToString() != "System.Byte[]")
                        {
                            if (col.DataType.ToString() == "System.String")
                            {
                                campossalida.Add(String.Format("QUOTENAME({0},'\"') AS {0}", col.ColumnName));
                            }
                            else
                            {
                                campossalida.Add(col.ColumnName);
                            }

                            campostabla.Add(col.ColumnName);
                            campostablatype.Add(col.ColumnName + " " + tipo(col.DataType.ToString()));
                            campostablainsert.Add("inserted." + col.ColumnName);
                            campovalores.Add(string.Format("source.{0}", col.ColumnName));
                            if (!Ckeys.Contains(col.ColumnName))
                            {
                                comparafiled += string.Format("(targe.{0} != source.{0} or (targe.{0} is null and source.{0} is not null)) or ", col.ColumnName);
                                igualafield += string.Format("{0} = source.{0},", col.ColumnName);
                            }
                        }
                        
                    }
                    if (igualafield.Length > 0)
                    {
                        igualafield = igualafield.Substring(0, igualafield.Length - 1);
                        comparafiled = comparafiled.Substring(0, comparafiled.Length - 3);
                    }
                    string comparakey = string.Empty;
                    foreach (String key in Ckeys)
                    {
                        comparakey += string.Format("targe.{0}=source.{0} and ", key);

                    }
                    if (comparakey.Length > 0)
                    {
                        comparakey = comparakey.Substring(0, comparakey.Length - 4);
                    }
                    
                    filename = creaarchivo(tablaname, campostabla, "D:\\Data WareHouse\\Data DW\\");
                    Info.actual = 0;
                   
                    List<String> lexisten = verificatabla2(tablaname, form, cn, cn2, ot, Ckeys, temporal, filename);

                    
                    string d1 = string.Empty, d2 = string.Empty;

                    if (tienekeys(tablaname,cn2))
                    {
                        d1 = string.Format("SET IDENTITY_INSERT FinancDW.dbo.{0} ON;", tablaname);
                        d2 = string.Format("SET IDENTITY_INSERT FinancDW.dbo.{0} OFF;", tablaname);
                    }
                    //qoutename

                    sql = string.Format("DECLARE @T TABLE( {8}, ACTION varchar(20) ); {6} MERGE FinancDW.dbo.{0} AS targe" +
                                       " Using (SELECT {1} FROM Financ.dbo.{0}) as source" +
                                       "({1}) on ({2}) when MATCHED AND ( {3} ) then " +
                                       "UPDATE SET {4} when not matched then " +
                                       " insert ({1}) values ({5}) " +
                                       " OUTPUT {9},$action INTO @T ; {7} SELECT {10},ACTION FROM @T;",
                                       tablaname,
                                       string.Join(",", campostabla), 
                                       comparakey
                                       ,comparafiled, 
                                       igualafield, 
                                       string.Join(",", campovalores),
                                       d1,
                                       d2,
                                       string.Join(",", campostablatype), 
                                       string.Join(",", campostablainsert),
                                       string.Join(",",campossalida));

                    


                    cmd.CommandText = sql;
                    try
                    {
                            DataTable dataTable = new DataTable();
                        
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            dataTable.Load(reader);
                            if (dataTable.Rows.Count > 0)
                            {
                                generaexcel(filename, dataTable,tablaname);
                            }else
                            {
                               
                            }
                            
                            ot.tablaservidor = dataTable.Rows.Count;

                        }
                        reader.Close();
                        ot.status = "Terminado";
                        form.pasardatos(ot);
                        
                    }
                    catch(Exception ex)
                    {
                        ot.tarea = ex.Message;
                        ot.status = "Terminado";
                        form.pasardatos(ot);
                    }

                }
                else
                {
                    ot.tarea = "No Tiene Primary Keys";
                    ot.status = "Terminado";
                    form.pasardatos(ot);
                }
            }
            if (filename != "") {


                File.Delete(filename + ".csv"); 
            }
            
        }

        public string tipo(string tp)
        {
            string retorna = "";
            switch (tp)
            {
                case "System.String":
                    retorna = "varchar(max)";
                    break;
                case "System.DateTime":
                    retorna = "datetime"; 
                    break;
                case "System.Int32":
                    retorna = "int";
                    break;
                case "System.Double":
                    retorna = "float";
                    break;
                case "System.DBNull":
                    retorna = "varchar(max)";
                    break;
                case "System.Byte":
                    retorna = "tinyint";
                    break;
                case "System.Byte[]":
                    retorna = "timestamp";
                    break;
                default:
                    retorna = "varchar(max)";
                    break;
            }
            return retorna;
        }
        


    
        public List<String> verificatabla2(String tablaname,Interface1 form,SqlConnection cn,SqlConnection cn2, 
            Otabla ot, List<String> Ckeys,DataTable temporal,String filename)
        {
            List<String> LRestorna = new List<string>();
            String nulos = string.Empty;
            String camposcomparacion = string.Empty;
            List<string> campostablainsert = new List<string>();

            List<String> campostabla = new List<string>();
            List<string> camposalida = new List<string>();

            foreach (DataColumn col in temporal.Columns)
            {

                if (col.DataType.ToString() != "System.Byte[]")
               {
                    campostabla.Add(col.ColumnName + " " + tipo(col.DataType.ToString()));
                    if (col.DataType.ToString() == "System.String")
                    {
                        camposalida.Add(String.Format("QUOTENAME({0},'\"') AS {0}", col.ColumnName));
                    }
                    else
                    {
                        camposalida.Add(col.ColumnName);
                    }
                    campostablainsert.Add("deleted."+col.ColumnName);
               }

            }



            foreach (String str in Ckeys)
            {
                nulos += "null,";
                camposcomparacion += string.Format("FinancDW.dbo.{0}.{1}=F.{1} and ", tablaname, str);
            }
            nulos = nulos.Substring(0, nulos.Length - 1);
            camposcomparacion = camposcomparacion.Substring(0, camposcomparacion.Length - 4);
            //SqlConnection cn22 = new SqlConnection(cn.ConnectionString);
            //cn22.Open();
            using (SqlCommand cmd2 = new SqlCommand())
            {
                Regex reg = new Regex("[^a-zA-Z0-9 ]");
                cmd2.Connection = cn2;
                cmd2.CommandType = System.Data.CommandType.Text;
                cmd2.CommandTimeout = 0;
                cmd2.CommandText = string.Format("DECLARE @T TABLE( {3}, ACTION  varchar(20)); delete from FinancDW.dbo.{0} " +
                                                 " OUTPUT {4},'DELETE' INTO @T " +
                                                 " where not exists(" +
                                                 " select {1} from Financ.dbo.{0} F where {2} );SELECT {5},ACTION FROM @T "
                                                 , tablaname,nulos,camposcomparacion,
                                                 string.Join(",", campostabla),
                                                 string.Join(",", campostablainsert),
                                                 string.Join(",",camposalida));
                //cmd2.ExecuteNonQuery();
                try
                {
                    DataTable dataTable = new DataTable();

                    SqlDataReader reader = cmd2.ExecuteReader();
                    if (reader.HasRows)
                    {
                        dataTable.Load(reader);
                        //generaexcel(filename, dataTable,tablaname);
                        ot.tablaservidor = dataTable.Rows.Count;

                    }
                    reader.Close();
                    ot.status = "Eliminados";
                    form.pasardatos(ot);

                }
                catch (Exception ex)
                {
                    ot.tarea = cmd2.CommandText.ToString();
                    ot.status = "Terminado";
                    form.pasardatos(ot);
                }

            }
            return LRestorna;
        }
        public static List<String> Camposkey(String tablaname, SqlConnection cc)
        {
            List<String> ckeys = new List<String>();
            String Sql_tablas = " SELECT COLUMN_NAME ";
            Sql_tablas += " FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE ";
            Sql_tablas += " WHERE TABLE_NAME='" + tablaname + "' AND OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1 ";
            Sql_tablas += "  union SELECT name FROM sys.columns WHERE[object_id] = object_id('" + tablaname + "') AND is_identity = 1; ";
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = cc;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = Sql_tablas;
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            ckeys.Add(reader.GetString(0));
                        }
                    }

                    reader.Close();
                }
            }


            return ckeys;
        }

        public Dictionary<string, List<DataRow>> GetDataTableDictionaryList(DataTable dt, List<String> Ckeys)
        {

            Dictionary<string, List<DataRow>> masterIndex = new Dictionary<string, List<DataRow>>();
            foreach (DataRow row in dt.Rows)
            {
                string key = string.Empty;
                foreach (string col in Ckeys)
                {
                    key += (row[col] == DBNull.Value ? "<NULL>" : row[col]) + "//";
                }
                key = key.Substring(0, key.Length - 2);
                if (masterIndex.ContainsKey(key))
                {
                    masterIndex[key].Add(row);
                }
                else
                {
                    masterIndex.Add(key, new List<DataRow>());
                    masterIndex[key].Add(row);
                }



            }
            return masterIndex;
        }
        public String creaarchivo(string tablaname, List<String> Campos, string filepath_grabar_excel)
        {
            String filename = tablaname;

            //Console.WriteLine("Copiando los Registros de la tabla {0}", filename);
            String fechas = DateTime.Now.ToString("ddMMyyyy"); //DateTime.Now.ToString("ddMMyyyyHHmmss");
            String horas = DateTime.Now.ToString("HHmmss");
            filename = String.Format("{0}{1}", filename, fechas + horas);
            StreamWriter mylogs = new StreamWriter(filepath_grabar_excel + filename + ".csv");


            filename =
                filepath_grabar_excel + filename;


            String s = "";
            for (int i1 = 0; i1 < Campos.Count; i1++)
            {
                s += Campos[i1] + ',';

            }
            s += "ACTION ";
            s = s.Substring(0, s.Length - 1); //eliminar la ultima coma
            mylogs.WriteLine(s);
            s = "";
            mylogs.Close();
            return filename;
        }
        public void insertaarchivo(string filepath_grabar_excel, DataRow row)
        {
            using (StreamWriter mylogs = File.AppendText(filepath_grabar_excel + ".csv"))
            {
                
                String s = "";
                for (int ii = 0; ii < row.Table.Columns.Count; ii++)
                {

                    s += row[row.Table.Columns[ii].ColumnName].ToString() + ',';

                }
                s = s.Substring(0, s.Length - 1); // eliminar la ultima coma
                mylogs.WriteLine(s);
                s = "";
                mylogs.Close();
             
            }
        }
        public bool tienekeys (string tablaname,SqlConnection cn)
        {
            //saber si tiene identity
            List<String> tablassinidentity = new List<string>();
            using (SqlCommand cmd_nokeys = new SqlCommand())
            { 
                bool respuesta = false;
                cmd_nokeys.Connection = cn;
                cmd_nokeys.CommandType = CommandType.Text;
                cmd_nokeys.CommandText = string.Format("SELECT	name, is_identity " +
                                         " FROM sys.columns " +
                                         "   WHERE[object_id] = object_id('{0}') " +
                                         "   AND is_identity = 1; ",tablaname);
                using (SqlDataAdapter da_nokeys = new SqlDataAdapter(cmd_nokeys))
                {
                    DataTable dt_nokeys = new DataTable();
                    da_nokeys.Fill(dt_nokeys);
                    
                    if (dt_nokeys.Rows.Count > 0)
                    {
                        respuesta = true;
                    }
                    da_nokeys.Dispose();
                }
                cmd_nokeys.Dispose();

                return respuesta;
              
            }
        }
        public void generaexcel(string filename, DataTable dt2,String tablaname)
        {
            //String filename = tablaname;

            ////Console.WriteLine("Copiando los Registros de la tabla {0}", filename);
            //String fechas = DateTime.Now.ToString("ddMMyyyy"); //DateTime.Now.ToString("ddMMyyyyHHmmss");
            //String horas = DateTime.Now.ToString("HHmmss");
            //filename = String.Format("{0}{1}", filename, fechas + horas);

            if (File.Exists(filename + ".csv"))
            {
                using (StreamWriter mylogs = File.AppendText(filename + ".csv"))
                {
                    String s = "";
                    //for (int i1 = 0; i1 < dt2.Columns.Count; i1++)
                    //{
                    //    s += dt2.Columns[i1].ColumnName + ',';

                    //}
                    //s = s.Substring(0, s.Length - 1); //eliminar la ultima coma
                    //mylogs.WriteLine(s);
                    //s = "";
                    for (int i2 = 0; i2 < dt2.Rows.Count; i2++)
                    {
                        for (int ii = 0; ii < dt2.Columns.Count; ii++)
                        {
                            DataRow row2 = dt2.Rows[i2];
                            String dd = row2[dt2.Columns[ii].ColumnName].GetType().ToString();
                            switch (dd)
                            {
                                case "System.DateTime":
                                    s += Convert.ToDateTime(row2[dt2.Columns[ii].ColumnName]).ToString("yyyy/MM/dd") + ',';
                                    break;
                                default:
                                    string myString = row2[dt2.Columns[ii].ColumnName].ToString();
                                    byte[] bytes = Encoding.Default.GetBytes(myString);
                                    myString = Encoding.UTF8.GetString(bytes);
                                    s += myString + ',';
                                    break;
                            }
                            //un cambio

                        }
                        s = s.Substring(0, s.Length - 1); // eliminar la ultima coma
                        mylogs.WriteLine(s);
                        s = "";
                    }
                    mylogs.Close();
                    if (dt2.Rows.Count > 0)
                    {
                        NameValueCollection nvc = new NameValueCollection();
                        nvc.Add("name", tablaname);
                        // nvc.Add("descripction", string.Format("Total de Registros {0}", dt2.Rows.Count));
                        //HttpUploadFile("https://file-uploaders.herokuapp.com/file_uploaders",
                        // HttpUploadFile("http://localhost:3000/drops",
                        HttpUploadFile("https://file-uploaders.herokuapp.com/file_uploaders",
                        string.Format(@"{0}.csv", filename), "file", "text/cvs", nvc);
                       


                    }
                }
            }
            //else
            //{
            //    using (StreamWriter mylogs = new StreamWriter(filepath_grabar_excel + filename + ".csv"))
            //    {
            //        String s = "";
            //        for (int i1 = 0; i1 < dt2.Columns.Count; i1++)
            //        {
            //            s += dt2.Columns[i1].ColumnName + ',';

            //        }
            //        s = s.Substring(0, s.Length - 1); //eliminar la ultima coma
            //        mylogs.WriteLine(s);
            //        s = "";
            //        for (int i2 = 0; i2 < dt2.Rows.Count; i2++)
            //        {
            //            for (int ii = 0; ii < dt2.Columns.Count; ii++)
            //            {
            //                DataRow row2 = dt2.Rows[i2];
            //                s += row2[dt2.Columns[ii].ColumnName].ToString() + ',';

            //            }
            //            s = s.Substring(0, s.Length - 1); // eliminar la ultima coma
            //            mylogs.WriteLine(s);
            //            s = "";
            //        }
            //        mylogs.Close();
            //    }

            //}



        }

        public static void HttpUploadFile(string url, string file, string paramName, string contentType, NameValueCollection nvc)
        {
            // log.Debug(string.Format("Uploading {0} to {1}", file, url));
            string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
            wr.ContentType = "multipart/form-data; boundary=" + boundary;
            wr.Method = "POST";
            wr.KeepAlive = true;
            wr.Credentials = System.Net.CredentialCache.DefaultCredentials;

            Stream rs = wr.GetRequestStream();

            string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";
            foreach (string key in nvc.Keys)
            {
                rs.Write(boundarybytes, 0, boundarybytes.Length);
                string formitem = string.Format(formdataTemplate, key, nvc[key]);
                byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                rs.Write(formitembytes, 0, formitembytes.Length);
            }
            rs.Write(boundarybytes, 0, boundarybytes.Length);

            string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";
            string header = string.Format(headerTemplate, paramName, file, contentType);
            byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);
            rs.Write(headerbytes, 0, headerbytes.Length);

            FileStream fileStream = new FileStream(file, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[4096];
            int bytesRead = 0;
            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                rs.Write(buffer, 0, bytesRead);
            }
            fileStream.Close();

            byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            rs.Write(trailer, 0, trailer.Length);
            rs.Close();

            WebResponse wresp = null;
            try
            {
                wresp = wr.GetResponse();
                Stream stream2 = wresp.GetResponseStream();
                StreamReader reader2 = new StreamReader(stream2);
                //log.Debug(string.Format("File uploaded, server response is: {0}", reader2.ReadToEnd()));
            }
            catch (Exception ex)
            {
                //log.Error("Error uploading file", ex);
                if (wresp != null)
                {
                    wresp.Close();
                    wresp = null;
                }
            }
            finally
            {
                wr = null;
            }
        }


    }
}
