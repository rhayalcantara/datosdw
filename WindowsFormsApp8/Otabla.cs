﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace datosdwform
{
    public class Otabla
    {
        //Stopwatch swra = new Stopwatch();
        //swra.Start();
        private static int _id = 0;
        private static float _totalprocesado;
        private static float _totalactualizado;
        private static float _pgtotal;
        private static float _pgactual;
        private static Stopwatch swra_global = new Stopwatch();
        private Stopwatch swra_local = new Stopwatch();
        private float _actual;
        private String _status;
        private String _tarea;
        public Otabla(int i) { }
        public Otabla()
        {
            _id++;
            id = _id;
            _status = "Iniciada";
            swra_local.Start();
            if (!swra_global.IsRunning)
            {
                swra_global.Start();
            }
            termino = false;

        }
        public int id { get; set; }
        public String nombre { get; set; }
        public float totaldatos { get; set; }
        public String tarea
        {
            get
            {
                return _tarea;
            }
            set
            {
                _tarea += value + "\n";
            }
        }
        public int tablalocal { get; set; }
        public int tablaservidor { get; set; }
        public String Tlocal { get { return swra_local.Elapsed.ToString(); } }
        public string Tglobal { get { return swra_global.Elapsed.ToString(); } }
        public String status
        {
            get
            {
                return _status;
            }
            set
            {

                if (value == "Terminado" && _status != value)
                {

                    _pgactual++;
                    swra_local.Stop();
                    if (_pgactual == _pgtotal)
                    {
                        swra_global.Stop();
                    }
                }
                _status = value;

            }
        }
        public String status_global
        {
            get
            {
                if (swra_global.IsRunning)
                {
                    return "En Proceso";
                }
                else
                {
                    return "Fin";
                }

            }
        }
        public float actual
        {
            get
            {
                return _actual;
            }
            set
            {
                _actual = value;
                _totalprocesado++;
            }
        }
        public bool termino { get; set; }
        public float pgtotal
        {
            get
            {
                return _pgtotal;
            }
            set
            {
                _pgtotal = value;
            }
        }

        public float Getpgactual()
        {
            return _pgactual;
        }
        public float GetDatosprosesados()
        {
            return _totalprocesado;
        }
        public float GetDatosActualizados()
        {
            return _totalactualizado;
        }
        public void SetDatosActualizados(float cant)
        {
            _totalactualizado += cant;
            //_tarea += " Se Actualizaron " + cant.ToString() + "\n";
        }

        public float GetDiffTabla()
        {
            float dif = tablaservidor - tablalocal;
            return dif;
        }
    }
}
