﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace datosdwform
{
    public class EjecutaComando
    {
        private SqlConnection cnx = new SqlConnection();
        private SqlCommand cmd = new SqlCommand();
        public EjecutaComando() { }
        
        public int Ejecuta(String sql,SqlConnection cn)
        {
            try
            {
                int retorna = 0;
                this.cnx = new SqlConnection(cn.ConnectionString);
                if (cnx.State == System.Data.ConnectionState.Closed)
                {
                    cnx.Open();
                }
                cmd.Connection = cnx;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = sql;
                retorna=cmd.ExecuteNonQuery();
                cmd.Dispose();
                cnx.Close();
                cnx.Dispose();
                return retorna;
            }catch(Exception ex)
            {
                throw ex;
            }
   

        }
        
    }
}
