﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace datosdwform
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {

            //Mutex instanceLock = new Mutex(false, "Nombre del Assembly");
            //if (instanceLock.WaitOne(0, false))
            //{
            //    try
            //    {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);

                    Application.Run(new Form1());
                    Application.Exit();
            //    }
            //    finally
            //    { instanceLock.ReleaseMutex(); }
            //}
            //else
            //{
            //    //MessageBox.Show("No se puede ejecutar 2 veces");
            //    Application.Exit();
            //}

            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());
         
        }
       
    }
}
